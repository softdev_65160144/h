/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab1;

/**
 *
 * @author Kim
 */

import java.util.Scanner;

public class Lab1 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
		System.out.println("WELCOME TO XO GAMES");
        char[][] board = new char[3][3];

         for(int i = 0; i < 3; i++) {
			for(int j = 0; j < 3; j++) {
				board[i][j] = '-';
			}
		}
        table(board);

        boolean xturn = true;
        int row = 0;
		int col = 0;
        while(true){
            char c = '-';
			if(xturn) {
				c = 'x';} 
            else {
				c = 'o';
			}
            if(xturn) {
				System.out.println("X Turn");} 
            else {
				System.out.println("O Turn");
			}
            System.out.println("Choose your row");
            row = in.nextInt();
            System.out.println("Choose your column");
            col = in.nextInt();

            board[row][col] = c;
            xturn = !xturn;
            table(board);

            if(playerHasWon(board) == 'x') {
				System.out.println(" X has won!");
                break;
			} else if(playerHasWon(board) == 'o') {
				System.out.println(" O has won!");
                break;
			}else{
				if(boardIsFull(board)) {
					System.out.println("It's a tie!");
					break;
				} 
			}
             
        }

      }
    //create board
    public static void table(char[][] board) {
        System.out.println("------------------------");
		for(int i = 0; i < 3; i++) {
			for(int j = 0; j < 3; j++) {
				System.out.print("     "+board[i][j]);
			}
			System.out.println();
		}
        System.out.println("------------------------");
	}
    //check
    public static char playerHasWon(char[][] board) {
		
		for(int i = 0; i < 3; i++) {
			if(board[i][0] == board[i][1] && board[i][1] == board[i][2] && board[i][0] != '-') {
				return board[i][0];
			}
		}

		for(int j = 0; j < 3; j++) {
			if(board[0][j] == board[1][j] && board[1][j] == board[2][j] && board[0][j] != '-') {
				return board[0][j];
			}
		}

		if(board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[0][0] != '-') {
			return board[0][0];
		}
		if(board[2][0] == board[1][1] && board[1][1] ==  board[0][2] && board[2][0] != '-') {
			return board[2][0];
		}

		return ' ';

	}
	public static boolean boardIsFull(char[][] board) {
		for(int i = 0; i < 3; i++) {
			for(int j = 0; j < 3; j++) {
				if(board[i][j] == '-') {
					return false;
				}
			}
		}
		return true;
	}

    
}
